using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

public interface IChatHub
{
    Task ReceiveMessage(string username, string message);
    Task GroupMessage(string username, string message);
}

public class ChatHub : Hub<IChatHub>
{
    public async Task SendMessage(string username, string message)
    {
        await Clients.All.ReceiveMessage(username, message);
    }

    public async Task AddToGroupMessage(string username, string message)
    {
        await Clients.All.GroupMessage(username, message);
    }
}