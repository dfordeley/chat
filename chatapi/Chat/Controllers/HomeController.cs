using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace Chat.Controllers
{
    [Route("/")]
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public HomeController() { }

        [HttpGet]
        public IActionResult Get()
        {
            var rng = new Random();
            return Ok(Summaries[rng.Next(Summaries.Length)]);
        }
    }
}
