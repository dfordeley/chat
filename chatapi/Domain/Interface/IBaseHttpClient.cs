using System.Threading.Tasks;

public interface IBaseHttpClient
{
    Task<string> PostAsync(string baseUrl, object postdata, string url, string token = null, string apikey = null);
    Task<T> PostAsync<T>(string baseUrl, object postdata, string url, string token = null, string apikey = null);
    Task<T> GetAsync<T>(string baseUrl, string url, string token = null, string apikey = null);
}
