public class Queries
{
    public static string GetStudentByParentID
    {
        get
        {
            return @"
                select 
                student.""FirstName"",
                student.""LastName"", 
                (
                    select
                    schoolClass.""Name"" 
                    from ""StudentClasses"" studentClass
                    inner join ""SchoolClasses"" schoolClass on studentClass.""SchoolClassId"" = schoolClass.""Id""
                    where studentClass.""StudentId"" = student.""Id"" and studentClass.""CurrentClass"" = true
                    and studentClass.""IsDeleted"" = false 
                ) as ""StudentClass"",
                (
                    select
                    studentClass.""Id"" as ""StudentClassId""
                    from ""StudentClasses"" studentClass
                    inner join ""SchoolClasses"" schoolClass on studentClass.""SchoolClassId"" = schoolClass.""Id""
                    where studentClass.""StudentId"" = student.""Id"" and studentClass.""CurrentClass"" = true
                    and studentClass.""IsDeleted"" = false
                ) as ""StudentClassId"",
        		school.""Name"" as ""SchoolName"",
                school.""Id"" as ""SchoolId"",
                school.""SchoolType"" as ""SchoolType"",
            	student.""Id"",
                student.""CreatedDate""
        		from ""SchoolStudents"" schoolStudent
        		inner join ""Students"" student on schoolStudent.""StudentId"" = student.""Id""
                inner join ""Schools"" school on schoolStudent.""SchoolId"" = school.""Id""
                where schoolStudent.""IsDeleted"" = false and student.""ParentId"" =@parentId";
        }
    }

    public static string GetStudentBySchoolID
    {
        get
        {
            return @"
                select 
                student.""FirstName"",
                student.""LastName"", 
                (
                    select
                    schoolClass.""Name"" 
                    from ""StudentClasses"" studentClass
                    inner join ""SchoolClasses"" schoolClass on studentClass.""SchoolClassId"" = schoolClass.""Id""
                    where studentClass.""StudentId"" = student.""Id"" and studentClass.""CurrentClass"" = true
                    and studentClass.""IsDeleted"" = false 
                ) as ""StudentClass"",
                (
                    select
                    studentClass.""Id"" 
                    from ""StudentClasses"" studentClass
                    inner join ""SchoolClasses"" schoolClass on studentClass.""SchoolClassId"" = schoolClass.""Id""
                    where studentClass.""StudentId"" = student.""Id"" and studentClass.""CurrentClass"" = true
                    and studentClass.""IsDeleted"" = false 
                ) as ""StudentClassId"",
                (
                    select
                    schoolClass.""Id"" as ""SchoolClassId""
                    from ""StudentClasses"" studentClass
                    inner join ""SchoolClasses"" schoolClass on studentClass.""SchoolClassId"" = schoolClass.""Id""
                    where studentClass.""StudentId"" = student.""Id"" and studentClass.""CurrentClass"" = true
                    and studentClass.""IsDeleted"" = false
                ) as ""SchoolClassId"",
            	student.""Id"",
                student.""CreatedDate""
        		from ""SchoolStudents"" schoolStudent
        		inner join ""Students"" student on schoolStudent.""StudentId"" = student.""Id""
                where schoolStudent.""IsDeleted"" = false and schoolStudent.""SchoolId"" =@schoolId";
        }
    }


    // public static string GetStudentBySchoolID
    // {
    //     get
    //     {
    //         return @"
    //             select 
    //             student.""FirstName"",
    //             student.""LastName"", 
    //             (
    //                 select
    //                 schoolClass.""Name"",
    //                 studentClass.""Id"",
    //                 schoolClass.""Id"" as ""SchoolClassId""
    //                 from ""StudentClasses"" studentClass
    //                 inner join ""SchoolClasses"" schoolClass on studentClass.""SchoolClassId"" = schoolClass.""Id""
    //                 where studentClass.""StudentId"" = student.""Id"" and studentClass.""CurrentClass"" = true
    //                 and studentClass.""IsDeleted"" = false 
    //             ) as ""StudentClass"",""StudentClassId"",""SchoolClassId"",
    //         	student.""Id"",
    //             student.""CreatedDate""
    //     		from ""SchoolStudents"" schoolStudent
    //     		inner join ""Students"" student on schoolStudent.""StudentId"" = student.""Id""
    //             where schoolStudent.""IsDeleted"" = false and schoolStudent.""SchoolId"" =@schoolId";
    //     }
    // }

    public static string GetStudentAssignments
    {
        get
        {
            return @"
                select 
                assignment.""Title"",
                assignment.""Instructions"",
                assignment.""FileType"",
                assignment.""UploadURL"",
                assignment.""SubmissionDate"",
                assignment.""CreatedDate"", 
                assignment.""Id"" as ""AssignmentId"",
        		subject.""Name"" as ""Subject"",
                subject.""Id"" as ""SubjectId""
        		from ""Assignments"" assignment
        		inner join ""Subjects"" subject on assignment.""SubjectId"" = subject.""Id""
                where assignment.""IsDeleted"" = false and assignment.""SchoolClassId"" =@schoolClassId
                and assignment.""Id"" not in 
                (
                    select 
                    submittedAssignments.""AssignmentId""
                    from ""SubmittedAssignments"" submittedAssignments
                    where submittedAssignments.""IsDeleted"" = false and submittedAssignments.""SchoolId"" = assignment.""SchoolId""
                )";
        }
    }

    public static string GetStudentAssignments2
    {
        get
        {
            return @"
                select 
                assignment.""Title"",
                assignment.""Instructions"",
                assignment.""FileType"",
                assignment.""UploadURL"",
                assignment.""SubmissionDate"",
                assignment.""CreatedDate"", 
                assignment.""Id"" as ""AssignmentId"",
        		subject.""Name"" as ""Subject"",
                subject.""Id"" as ""SubjectId""
        		from ""Assignments"" assignment
        		inner join ""Subjects"" subject on assignment.""SubjectId"" = subject.""Id""
                where assignment.""IsDeleted"" = false and assignment.""SchoolClassId"" =@schoolClassId
                and assignment.""Id"" not in 
                (
                    select 
                    submittedAssignments.""AssignmentId""
                    from ""SubmittedAssignments"" submittedAssignments
                    where submittedAssignments.""IsDeleted"" = false and submittedAssignments.""SchoolId"" = assignment.""SchoolId""
                    and submittedAssignments.""StudentId"" =@studentId
                )";
        }
    }

    public static string GetStudentAssignments3
    {
        get
        {
            return @"
                select 
                assignment.""Title"",
                assignment.""Instructions"",
                assignment.""FileType"",
                assignment.""UploadURL"",
                assignment.""SubmissionDate"",
                assignment.""CreatedDate"", 
                assignment.""Id"" as ""AssignmentId"",
        		subject.""Name"" as ""Subject"",
                subject.""Id"" as ""SubjectId""
        		from ""Assignments"" assignment
        		inner join ""Subjects"" subject on assignment.""SubjectId"" = subject.""Id""
                where assignment.""IsDeleted"" = false and assignment.""SchoolClassId"" =@schoolClassId
                and assignment.""Id"" not in 
                (
                    select 
                    submittedAssignments.""AssignmentId""
                    from ""SubmittedAssignments"" submittedAssignments
                    where submittedAssignments.""IsDeleted"" = false and submittedAssignments.""SchoolId"" = assignment.""SchoolId""
                    and submittedAssignments.""StudentId"" =@studentId
                )
                and assignment.""Id"" not in 
                (
                    select 
                    studentQuizTrackers.""AssignmentId""
                    from ""StudentQuizTrackers"" studentQuizTrackers
                    where studentQuizTrackers.""IsDeleted"" = false 
                    and studentQuizTrackers.""EndTime"" < now()  or studentQuizTrackers.""IsEnded"" = true
                    and studentQuizTrackers.""StudentId"" =@studentId
                )";
        }
    }

    public static string GetCountStudentAssignments
    {
        get
        {
            return @"
                select count(*)
                assignment.""Id"" as ""AssignmentId"",
        		from ""Assignments"" assignment
        		inner join ""Subjects"" subject on assignment.""SubjectId"" = subject.""Id""
                where assignment.""IsDeleted"" = false and assignment.""SchoolClassId"" =@schoolClassId
                and assignment.""Id"" not in 
                (
                    select 
                    submittedAssignments.""AssignmentId""
                    from ""SubmittedAssignments"" submittedAssignments
                    where submittedAssignments.""IsDeleted"" = false and submittedAssignments.""SchoolId"" = assignment.""SchoolId""
                    and submittedAssignments.""StudentId"" =@studentId
                )";
        }
    }

    public static string UpdateQuestionOptions
    {
        get
        {
            return @"
            update ""QuestionOptions"" set ""Entry""=@entry, ""IsCorrect""=@isCorrect, ""ModifiedDate""=now() where ""Id""=@optionId
            ";
        }
    }

    public static string UpdateQuestion
    {
        get
        {
            return @"
            update ""Questions"" set ""Entry""=@entry, ""ModifiedDate""=now() where ""Id""=@questionId
            ";
        }
    }

    public static string DeleteQuestionOptions
    {
        get
        {
            return @"
            update ""QuestionOptions"" set ""IsDeleted""=true, ""ModifiedDate""=now(), ""Deleted""=now() where ""Id""=@optionId
            ";
        }
    }

    public static string DeleteQuestion
    {
        get
        {
            return @"
            update ""Questions"" set ""IsDeleted""=true, ""ModifiedDate""=now(), ""Deleted""=now() where ""Id""=@questionId
            ";
        }
    }
}