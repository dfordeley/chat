using System.ComponentModel.DataAnnotations;


public enum TokenType
{
    Validation = 1,
    RefreshToken = 2,
    SchoolAdminSignUp = 3,
    TeacherSignUp = 4,
    ParentSignUp = 5
}

public enum Gender
{
    Male = 1,
    Female
}

public enum UploadType
{
    Lecture,
    Assignment,
    Submission
}

public enum FileType
{
    video = 0,
    image = 1,
    audio = 2,
    pdf = 3,
    word = 4,
    powerpoint = 5,
    text = 6
}

public enum AssignmentType
{
    Theory,
    MultiChoice
}

public enum CorrectOption
{
    Option_One = 1,
    Option_Two,
    Option_Three,
    Option_Four
}

public enum ParticipantType
{
    Individual,
    Group
}

public enum MessageType
{
    Direct,
    Reply
}

