using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

public static class Utilities
{
    public static string MoneyFormatter(decimal? Amount, bool addSign = false)
    {
        string formattedAmnt = Amount < 0 || Amount == null ? "0.00" : Convert.ToDecimal(Amount).ToString("#,##0.00");
        return addSign ? formattedAmnt : $"&#x20A6; {formattedAmnt}";
    }

    public static string Ref(int lenght = 10, bool isWithTimeStamp = false)
    {
        var reference = $"{Guid.NewGuid().ToString().Replace("-", "").Remove(lenght).ToUpper()}";
        return isWithTimeStamp ? $"{DateTime.Now:ssmmhhddMMyy}{reference}" : reference;
    }

    public static T NewTonDeserializeJson<T>(string input)
    {
        // return JsonConvert.DeserializeObject<T>(input);
        return JsonSerializer.Deserialize<T>(json: input);
    }

    public static string NewTonSerializeJson(object input)
    {
        // return JsonConvert.SerializeObject(input);
        return JsonSerializer.Serialize(input);
    }

    public static T DeserializeJson<T>(string input)
    {
        return JsonSerializer.Deserialize<T>(json: input);
    }

    public static string SerializeJson(object input)
    {
        return JsonSerializer.Serialize(input);
    }

    public static string SerializeJsonAuto(this object input)
    {
        return System.Text.Json.JsonSerializer.Serialize(input);
    }

    private static readonly JsonSerializerOptions Options = new JsonSerializerOptions();
    public static async Task<T> DeserializeRequestAsync<T>(HttpResponseMessage response)
    {
        var contentStream = await response.Content.ReadAsStreamAsync();
        var result = await System.Text.Json.JsonSerializer.DeserializeAsync<T>(contentStream, Options);
        return result;
    }

    public static bool IsValidEmail(this string email)
    {
        try
        {
            var addr = new MailAddress(email);
            if (addr.Address != email) return false;

            var split = email.Split('@')[1];
            if (!split.Contains('.')) return false;

            return true;
        }
        catch
        {
            return false;
        }
    }

    public static bool IsAcceptedImage(string imageType)
    {
        var acceptedImageType = new List<string>
        {
            "image/jpg",
            "image/jpeg",
            "image/png"
        };

        return acceptedImageType.Any(x => x == imageType);
    }

    public static bool IsAcceptedAudio(string audioFormat)
    {
        var acceptedImageType = new List<string>
        {
            "audio/x-ms-wma","audio/x-wav","audio/wav","audio/mpeg","audio/mp3","audio/x-ms-wmv"
        };

        return acceptedImageType.Any(x => x == audioFormat);
    }

    public static bool IsAcceptedVideo(string videoFormat)
    {
        var acceptedImageType = new List<string>
        {
            "video/mp4","video/avi","video/mpeg","video/mpeg"
        };

        return acceptedImageType.Any(x => x == videoFormat);
    }

    public static bool IsAcceptedWordDoc(string wordFileType)
    {
        var acceptedImageType = new List<string>
        {
            "application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        };

        return acceptedImageType.Any(x => x == wordFileType);
    }

    public static bool IsAcceptedPowerPointDoc(string powerPointDocType)
    {
        var acceptedImageType = new List<string>
        {
            "application/vnd.ms-powerpoint",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation"
        };

        return acceptedImageType.Any(x => x == powerPointDocType);
    }

    public static string TrimAllSpace(this string value)
    {
        return Regex.Replace(value, @"\s+", "");
    }

    public static string GetEncryptionKey(string secretKey)
    {
        MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
        byte[] secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
        byte[] hashedSecret = md5.ComputeHash(secretKeyBytes, 0, secretKeyBytes.Length);
        byte[] hashedSecretLast12Bytes = new byte[12];
        Array.Copy(hashedSecret, hashedSecret.Length - 12, hashedSecretLast12Bytes, 0, 12);
        String hashedSecretLast12HexString = BitConverter.ToString(hashedSecretLast12Bytes);
        hashedSecretLast12HexString = hashedSecretLast12HexString.ToLower().Replace("-", "");
        String secretKeyFirst12 = secretKey.Replace("FLWSECK-", "").Substring(0, 12);
        byte[] hashedSecretLast12HexBytes = Encoding.UTF8.GetBytes(hashedSecretLast12HexString);
        byte[] secretFirst12Bytes = Encoding.UTF8.GetBytes(secretKeyFirst12);
        byte[] combineKey = new byte[24];
        Array.Copy(secretFirst12Bytes, 0, combineKey, 0, secretFirst12Bytes.Length);
        Array.Copy(hashedSecretLast12HexBytes, hashedSecretLast12HexBytes.Length - 12, combineKey, 12, 12);
        return Encoding.UTF8.GetString(combineKey);
    }

    public static string EncryptData(string encryptionKey, string data)
    {
        using TripleDES des = new TripleDESCryptoServiceProvider
        {
            Mode = CipherMode.ECB,
            Padding = PaddingMode.PKCS7,
            Key = Encoding.UTF8.GetBytes(encryptionKey)
        };
        ICryptoTransform cryptoTransform = des.CreateEncryptor();
        byte[] dataBytes = Encoding.UTF8.GetBytes(data);
        byte[] encryptedDataBytes = cryptoTransform.TransformFinalBlock(dataBytes, 0, dataBytes.Length);
        des.Clear();
        return Convert.ToBase64String(encryptedDataBytes);
    }

    public static string DecryptData(string encryptedData, string encryptionKey)
    {
        using TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider
        {
            Key = Encoding.UTF8.GetBytes(encryptionKey),
            Mode = CipherMode.ECB,
            Padding = PaddingMode.PKCS7
        };
        ICryptoTransform cryptoTransform = des.CreateDecryptor();
        byte[] EncryptDataBytes = Convert.FromBase64String(encryptedData);
        byte[] plainDataBytes = cryptoTransform.TransformFinalBlock(EncryptDataBytes, 0, EncryptDataBytes.Length);
        des.Clear();
        return Encoding.UTF8.GetString(plainDataBytes);
    }

    public static void Forget(this Task task)
    {
        if (!task.IsCompleted || task.IsFaulted) _ = ForgetAwaited(task);

        async static Task ForgetAwaited(Task task)
        {
            try { await task.ConfigureAwait(false); }
            catch {/*Nothing to do here*/}
        }
    }

    public static string ToTitleCase(this string input)
    {
        return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(input);
    }

    public static List<T> Shuffle<T>(this List<T> list)
    {
        var rng = new Random();
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
        return list;
    }

    public static string Name(this Enum e)
    {
        try
        {
            var attributes = (DisplayAttribute[])e.GetType().GetField(e.ToString()).GetCustomAttributes(typeof(DisplayAttribute), false);
            return attributes.Length > 0 ? attributes[0].Name : string.Empty;
        }
        catch
        {
            return "NA";
        }
    }

    public static List<string> HostedSlug => new List<string> { "localhost", "www", "clickschools" };
}
