using System;

public static class Constants
{
    public static string ConnectionString => "Host=localhost;Database=clks.chat;Username=postgres;Password=adminp";

    //single sign on
    public static string SSOGetUserInfoURL => "api/access/account/get-details-by/";
    public static string SSOCreatUserURL => "api/access/create";
    public static string SSOSetProfilePassword => "api/access/newpassword";
    public static string SSOUserInfoURL => "api/access/getuserinfo";
    public static string SSOGetTokenURL => "api/token/get";
    public static string SSOPasswordRecoveryURL => "api/access/passwordrecovery";
    public static string SSOSetPasswordURL => "api/access/setpassword";
    public static string SSOChangePasswordURL => "api/access/changepassword";
    public static string SSOValidateAccountEmail => "api/access/account/emailcheck";
    public static string SSOUpdateUserAccess => "api/access/updateuser";
    public static string SSOUpdateUserAccessGetDetails => "api/access/updateuser-get-full-details";


    //send in blue
    public static string SendInBlueSendEmailURL => "v3/smtp/email";

    //public urls
    public static string SuperAdminFronendURL => "FrontendURL:SuperAdminURL";
    public static string VendorAdminFronendURL => "FrontendURL:VendorAdminURL";
    public static string SchoolAdminFronendURL => "FrontendURL:SchoolAdminURL";

    //aws s3 bucket
    public static string AWSS3PassportFolderPath => "passport";
    public static string AWSS3LogoFolderPath => "logo";

    public static string BugsNagKey => "777220070f14f6c7c66ab40f25fc6202";

    //aws sqs
    public static string AWSSQSUserIdConfigPath => "AWSSQS:UserID";
    public static string AWSSQSEmailQueueNameConfigPath => "AWSSQS:EmailQueueName";
    public static string AWSSQSAccesskeyConfigPath => "AWSSQS:Accesskey";
    public static string AWSSQSSecretkeyConfigPath => "AWSSQS:Secretkey";
    public static string AWSSQSRegionEndpointConfigPath => "AWSSQS:RegionEndpoint";


    //flutterwave
    public static string FlutterWaveVerifyPayURL => "flwv3-pug/getpaidx/api/v2/verify/";
    public static string FlutterWaveRefundURL => "gpx/merchant/transactions/refund/";
    public static string FlutterWaveInitiateTransferURL => "v2/gpx/transfers/create/";
    public static string FlutterWaveCallBackURL => "payout/callback/";
    public static string FlutterWaveResolveBankAccountURL => "flwv3-pug/getpaidx/api/resolve_account";
    public static string FlutterWaveCardCollectionURL => "flwv3-pug/getpaidx/api/charge";
    public static string FlutterWaveCardRequestConstant => "3DES-24";
    public static string FlutterWaveCardCollectionVerificationURL => "flwv3-pug/getpaidx/api/validatecharge";

    public static string DefaultSchoolLogoURL => "sample-school-logo.png";

    //get files from web root
    public static string BanksJsonAddress => "/files/ListBanks.json";
    public static string NigerianStateJson => "/files/ListNigerianStates.json";
    public static string CountriesJson => "/files/ListCountries.json";

    public static string EmailCodeForPaymentVerificationPath => "EmailSettings:PaymentVerification";
    public static string EmailCodeForResetPasswordPath => "EmailSettings:ResetPassword";
    public static string EmailAPIBaseURLPath => "APIURL:EmailBaseURL";
    public static string SendEmailURL => "send/email";

    //postmark app 
    public static string PostMarkKeyPath => "PostMark:Key";
}
