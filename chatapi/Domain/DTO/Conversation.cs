using System;

public class Conversation : Entity
{
    public string Title { get; set; }

    //user Id of the conversation starter
    public Guid CreatorId { get; set; }
    public virtual User Creator { get; set; }
}

public class ConversationModel : ViewModel
{
    public string Title { get; set; }
    public Guid CreatorId { get; set; }
}