public class School : Entity
{
    public string Name { get; set; }
    public string Code { get; set; }
}

public class SchoolModel : ViewModel
{
    public string Name { get; set; }
    public string Code { get; set; }
}