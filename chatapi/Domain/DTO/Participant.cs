using System;

public class Participant : Entity
{
    public Guid UserId { get; set; }
    public virtual User User { get; set; }

    public Guid ConversationId { get; set; }
    public virtual Conversation Conversation { get; set; }

    public ParticipantType Type { get; set; }
}

public class ParticipantModel : ViewModel
{
    public Guid UserId { get; set; }
    public Guid ConversationId { get; set; }
    public ParticipantType Type { get; set; }
}