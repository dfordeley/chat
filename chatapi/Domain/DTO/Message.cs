using System;

public class Message : Entity
{
    public string Content { get; set; }
    public MessageType Type { get; set; }

    public Guid SenderId { get; set; }
    public virtual User Sender { get; set; }
    public Guid ConversationId { get; set; }
    public virtual Conversation Conversation { get; set; }
}

public class MessageModel : ViewModel
{
    public string Content { get; set; }
    public MessageType Type { get; set; }

    //SenderId
    public Guid SenderId { get; set; }
    public Guid ConversationId { get; set; }
}