using System;

public class User : Entity
{
    public string FullName { get; set; }
    public string Email { get; set; }
    public string UserCode { get; set; }

    public Guid SchoolId { get; set; }
    public virtual School School { get; set; }
}

public class UserModel : ViewModel
{
    public string FullName { get; set; }
    public string Email { get; set; }
    public string UserCode { get; set; }

    public Guid SchoolId { get; set; }
}