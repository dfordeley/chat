using System;

public abstract class ViewModel : IViewModel
{
    public Guid Id { get; set; }
    public DateTime CreatedDate { get; set; }
}
