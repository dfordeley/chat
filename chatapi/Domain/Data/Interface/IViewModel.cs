using System;
public interface IViewModel
{
    Guid Id { get; set; }
    DateTime CreatedDate { get; set; }
}
