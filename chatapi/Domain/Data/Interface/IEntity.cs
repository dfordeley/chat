﻿using System;
public interface IEntity
{
    Guid Id { get; set; }
    DateTime CreatedDate { get; set; }
    DateTime? ModifiedDate { get; set; }
    string CreatedBy { get; set; }
    string ModifiedBy { get; set; }
    // byte[] Version { get; set; }
    DateTime? Deleted { get; set; }
    bool IsDeleted { get; set; }
}

