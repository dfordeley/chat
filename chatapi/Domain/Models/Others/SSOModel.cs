using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

public class SSOLoginModel : AppIdentityTenant
{
    [Required]
    [EmailAddress]
    public string Email { get; set; }

    [Required]
    [DataType(DataType.Password)]
    public string Password { get; set; }
}

public class LoginModel
{
    [Required(ErrorMessage = "email address is required")]
    [MaxLength(50, ErrorMessage = "Your email address should not be more than 50 characters")]
    [EmailAddress]
    public string Email { get; set; }

    [Required(ErrorMessage = "Password is required")]
    [DataType(DataType.Password)]
    public string Password { get; set; }
}

public class CreateAccountModel
{
    [Required(ErrorMessage = "Password is required")]
    [MaxLength(50, ErrorMessage = "Fullname should  not be more than 50 characters")]
    public string FullName { get; set; }

    [Required(ErrorMessage = "email address is required")]
    [MaxLength(50, ErrorMessage = "Your email address should not be more than 50 characters")]
    [EmailAddress]
    public string Email { get; set; }

    [Required(ErrorMessage = "Password is required")]
    [DataType(DataType.Password)]
    public string Password { get; set; }

    [Required(ErrorMessage = "Password is required")]
    [DataType(DataType.Password)]
    public string ConfirmPassword { get; set; }
}

public class AppIdentityTenant
{
    [JsonPropertyName("appId")]
    public string AppId { get; set; }
}

public class SSOUserModel : AppIdentityTenant
{
    public string Email { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string PhoneNumber { get; set; }
    public string UserCode { get; set; }
    public string Password { get; set; }
    public string ConfirmPassword { get; set; }
    public bool IsPasswordSet { get; set; }
}

public class SSOUserModelResponse
{
    public string Email { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string PhoneNumber { get; set; }
    public object Gender { get; set; }
    public Guid UserCode { get; set; }
    public object Address { get; set; }
    public bool IsPasswordSet { get; set; }
    public Guid AppId { get; set; }
}

public class CurrentUserModel
{
    public string Email { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string PhoneNumber { get; set; }
    public Guid UserCode { get; set; }
    public Guid SchoolId { get; set; }
    public string SchoolName { get; set; }
    public Guid ParentId { get; set; }
    public Guid AdminId { get; set; }
    public Guid TeacherId { get; set; }
}

public class TokenResponse
{
    [JsonPropertyName("accessToken")]
    public string AccessToken { get; set; }

    [JsonPropertyName("expiresAt")]
    public int ExpiresAt { get; set; }

    [JsonPropertyName("refreshToken")]
    public string RefreshToken { get; set; }

    [JsonPropertyName("refreshTokenExpiresAt")]
    public int RefreshTokenExpiresAt { get; set; }
}

public class PublicTokenResponse
{
    [JsonPropertyName("accessToken")]
    public string AccessToken { get; set; }

    [JsonPropertyName("expiresAt")]
    public int ExpiresAt { get; set; }

    [JsonPropertyName("refreshToken")]
    public string RefreshToken { get; set; }

    [JsonPropertyName("refreshTokenExpiresAt")]
    public string RefreshTokenExpiresAt { get; set; }
}


public class ChangePasswordModel
{
    [Required]
    [DataType(DataType.Password)]
    [Display(Name = "Current password")]
    public string OldPassword { get; set; }

    [Required]
    [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
    [DataType(DataType.Password)]
    [Display(Name = "New password")]
    public string NewPassword { get; set; }

    [DataType(DataType.Password)]
    [Display(Name = "Confirm new password")]
    [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
    public string ConfirmPassword { get; set; }

    public string Token { get; set; }
}

public class ResetPasswordModel : AppIdentityTenant
{
    [Required(ErrorMessage = "New Password is required")]
    [StringLength(99, ErrorMessage = "your password should have a minimum of 7 characters", MinimumLength = 7)]
    [DataType(DataType.Password)]
    public string NewPassword { get; set; }

    [DataType(DataType.Password)]
    [Compare("NewPassword", ErrorMessage = "Password and confirm password did not match")]
    public string ConfirmNewPassword { get; set; }

    [Required(ErrorMessage = "Token is required")]
    public string UserToken { get; set; }
}

public class ResetPasswordModelNew
{
    [Required(ErrorMessage = "New Password is required", AllowEmptyStrings = false)]
    [StringLength(99, ErrorMessage = "your password should have a minimum of 7 characters", MinimumLength = 7)]
    [DataType(DataType.Password)]
    public string NewPassword { get; set; }

    [DataType(DataType.Password)]
    [Compare("NewPassword", ErrorMessage = "Password and confirm password did not match")]
    public string ConfirmNewPassword { get; set; }

    [Required(ErrorMessage = "Token is required")]
    public string UserToken { get; set; }

    [Required(ErrorMessage = "Email is required", AllowEmptyStrings = false)]

    public string Email { get; set; }
}

public class SetPasswordModel : AppIdentityTenant
{
    [Required]
    [StringLength(99, ErrorMessage = "your password must have a Minimum Length of 7 characters.",
       MinimumLength = 7)]
    [DataType(DataType.Password)]
    public string NewPassword { get; set; }

    [DataType(DataType.Password)]
    [Compare("NewPassword", ErrorMessage = "Password and confirmation password did not match.")]
    public string ConfirmNewPassword { get; set; }

    [Required]
    public string UserCode { get; set; }
}

public class UpdateAccount
{
    public string Email { get; set; }
    public string AppId { get; set; }
}

public class PasswordRecoveryModel : AppIdentityTenant
{
    public string Email { get; set; }
}

public class PasswordRecovery
{
    [Required(ErrorMessage = "user email is required for password recovery")]
    public string Email { get; set; }
}