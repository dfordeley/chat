public class SSOSettings
{
    public string SSOId { get; set; }
    public string SSOBaseURL { get; set; }
}
