using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

public interface IResponseService
{
    ServiceResponse<T> ErroResponse<T>(string message) where T : class;
    ServiceResponse<T> SuccessResponse<T>(string message, T data) where T : class;
    ServiceResponse<T> ExceptionResponse<T>(Exception exception) where T : class;
    Task<ServiceResponse<PagedList<T>>> PagedSuccessResponseAsync<T>(IQueryable<T> source,
        int pageNumber, int pageSize, string entityName) where T : class;
}

public class ResponseService : IResponseService
{
    public ServiceResponse<T> ErroResponse<T>(string message)
        where T : class => new ServiceResponse<T>
        {
            Message = message,
            Status = false,
            Data = null
        };

    public ServiceResponse<T> ExceptionResponse<T>(Exception exception)
        where T : class => new ServiceResponse<T>
        {
            Message = exception.Message,
            Status = false,
            Data = null
        };

    public ServiceResponse<T> SuccessResponse<T>(string message, T data)
        where T : class => new ServiceResponse<T>
        {
            Message = message,
            Status = true,
            Data = data
        };

    public async Task<ServiceResponse<PagedList<T>>> PagedSuccessResponseAsync<T>(IQueryable<T> source,
        int pageNumber, int pageSize, string entityName) where T : class
    {

        var count = await source.CountAsync();
        var items = await source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
        var pagedResp = new PagedList<T>(items, count, pageNumber, pageSize, entityName);
        return new ServiceResponse<PagedList<T>>
        {
            Message = pagedResp.Summary,
            Status = true,
            Data = pagedResp
        };
    }
}