using System;
using System.Threading.Tasks;

/// <summary>
/// Base Service Generic Class
/// </summary>
/// <typeparam name="TEntity"> Domain Entity</typeparam>
/// <typeparam name="TDataObject"> Data Object</typeparam>
public interface IBaseService<TEntity, TDataObject>
{

}
