public class BaseService<TEntity, TDataObject> : IBaseService<TEntity, TDataObject> where TEntity : class, IEntity
{
    protected readonly IUnitOfWork _unitOfWork;
    protected readonly IResponseService _responseService;

    public BaseService(IUnitOfWork unitOfWork, IResponseService responseService)
    {
        _unitOfWork = unitOfWork;
        _responseService = responseService;
    }
}
