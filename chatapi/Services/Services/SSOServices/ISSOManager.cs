﻿using System.Threading.Tasks;

public interface ISSOManager
{
    Task<ServiceResponse<TokenResponse>> GetToken(LoginModel model);
    Task<ServiceResponse<CurrentUserModel>> GetUserDetailByUsercode(string userCode);
    Task<ServiceResponse<string>> CreateUserAsync(SSOUserModel model);
    Task<ServiceResponse<string>> PasswordRecovery(string email);
    Task<ServiceResponse<string>> ChangePasswordAsync(ChangePasswordModel model);
    Task<ServiceResponse<CurrentUserModel>> SetPasswordAsync(ResetPasswordModel model);
    Task<ServiceResponse<string>> CreateProfilePassword(SetPasswordModel model);
    Task<ServiceResponse<bool>> VerifyAccountEmail(string email);
    Task<ServiceResponse<string>> UpdateProfileAccess(string email);
    Task<ServiceResponse<CurrentUserModel>> UpdateProfileAccessGetDetails(string email);
}

