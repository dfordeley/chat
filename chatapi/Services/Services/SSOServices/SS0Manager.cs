﻿using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

public class SSOManager : ISSOManager
{
    private readonly IOptions<SSOSettings> _appSettings;
    private readonly IBaseHttpClient _httpClient;

    public SSOManager(IOptions<SSOSettings> appSettings, IBaseHttpClient httpClient)
    {
        _appSettings = appSettings;
        _httpClient = httpClient;
    }

    public async Task<ServiceResponse<TokenResponse>> GetToken(LoginModel model)
    {
        var ssoLogin = new SSOLoginModel
        {
            Email = model.Email,
            Password = model.Password,
            AppId = _appSettings.Value.SSOId
        };

        var response = await _httpClient.PostAsync<ServiceResponse<TokenResponse>>(
        baseUrl: _appSettings.Value.SSOBaseURL, postdata: ssoLogin, url: Constants.SSOGetTokenURL);
        return response;
    }

    public async Task<ServiceResponse<string>> CreateUserAsync(SSOUserModel model)
    {
        model.AppId = _appSettings.Value.SSOId;
        return await _httpClient.PostAsync<ServiceResponse<string>>(
           baseUrl: _appSettings.Value.SSOBaseURL,
           postdata: model,
           url: Constants.SSOCreatUserURL);
    }

    public async Task<ServiceResponse<CurrentUserModel>> GetUserDetailByUsercode(string userCode)
    {
        return await _httpClient.PostAsync<ServiceResponse<CurrentUserModel>>(
                baseUrl: _appSettings.Value.SSOBaseURL,
                postdata: null,
                url: $"{Constants.SSOGetUserInfoURL}{userCode}");
    }

    public async Task<ServiceResponse<string>> PasswordRecovery(string email)
    {
        var model = new PasswordRecoveryModel { Email = email, AppId = _appSettings.Value.SSOId };
        return await _httpClient.PostAsync<ServiceResponse<string>>(
                    baseUrl: _appSettings.Value.SSOBaseURL, postdata: model, url: Constants.SSOPasswordRecoveryURL);
    }

    public async Task<ServiceResponse<string>> ChangePasswordAsync(ChangePasswordModel model)
    {
        return await _httpClient.PostAsync<ServiceResponse<string>>(
           baseUrl: _appSettings.Value.SSOBaseURL,
           postdata: model,
           url: Constants.SSOChangePasswordURL, token: model.Token);

    }

    public async Task<ServiceResponse<CurrentUserModel>> SetPasswordAsync(ResetPasswordModel model)
    {

        model.AppId = _appSettings.Value.SSOId;
        return await _httpClient.PostAsync<ServiceResponse<CurrentUserModel>>(
           baseUrl: _appSettings.Value.SSOBaseURL,
           postdata: model,
           url: Constants.SSOSetPasswordURL);

    }

    public async Task<ServiceResponse<string>> CreateProfilePassword(SetPasswordModel model)
    {

        model.AppId = _appSettings.Value.SSOId;
        return await _httpClient.PostAsync<ServiceResponse<string>>(
           baseUrl: _appSettings.Value.SSOBaseURL,
           postdata: model,
           url: Constants.SSOSetProfilePassword);
    }

    public async Task<ServiceResponse<bool>> VerifyAccountEmail(string email)
    {
        var model = new PasswordRecoveryModel
        {
            Email = email,
            AppId = _appSettings.Value.SSOId
        };

        return await _httpClient.PostAsync<ServiceResponse<bool>>(
            baseUrl: _appSettings.Value.SSOBaseURL,
            postdata: model,
            url: Constants.SSOValidateAccountEmail);

        // if (!resp.Status) return new ServiceResponse<bool> { Message = "An internal error 3" };
        // var dataStat = Convert.ToBoolean(resp.Data);
        // return new ServiceResponse<bool> { Status = true, Message = resp.Message, Data = dataStat };
    }

    public async Task<ServiceResponse<string>> UpdateProfileAccess(string email)
    {

        var model = new UpdateAccount { Email = email, AppId = _appSettings.Value.SSOId };
        return await _httpClient.PostAsync<ServiceResponse<string>>(
           baseUrl: _appSettings.Value.SSOBaseURL,
           postdata: model,
           url: Constants.SSOUpdateUserAccess);
    }

    public async Task<ServiceResponse<CurrentUserModel>> UpdateProfileAccessGetDetails(string email)
    {

        var model = new UpdateAccount { Email = email, AppId = _appSettings.Value.SSOId };
        return await _httpClient.PostAsync<ServiceResponse<CurrentUserModel>>(
           baseUrl: _appSettings.Value.SSOBaseURL,
           postdata: model,
           url: Constants.SSOUpdateUserAccessGetDetails);
    }
}
