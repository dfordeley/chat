using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Services.Database
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<ChatDBContext>
    {
        public ChatDBContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ChatDBContext>();
            builder.UseNpgsql(Constants.ConnectionString);
            return new ChatDBContext(builder.Options);
        }
    }
}
