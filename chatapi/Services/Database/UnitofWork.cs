using System;
using System.Threading.Tasks;

namespace Services.Database
{
    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        public ChatDBContext Context { get; }
        private bool _disposed;

        public UnitOfWork(ChatDBContext context)
        {
            Context = context;
            _disposed = false;
        }

        public async Task CommitAsync()
        {
            await Context.SaveChangesAsync();
        }

        public void Commit()
        {
            Context.SaveChanges();
        }

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
            {
                Context.Dispose();
            }
            _disposed = true;
        }

        #endregion
    }
}
