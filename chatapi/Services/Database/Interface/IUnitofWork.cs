using System;
using System.Threading.Tasks;

public interface IUnitOfWork : IDisposable
{
    ChatDBContext Context { get; }
    Task CommitAsync();
    void Commit();
}
