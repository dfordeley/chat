using Microsoft.EntityFrameworkCore;

public class ChatDBContext : DbContext
{
    public ChatDBContext(DbContextOptions<ChatDBContext> options) : base(options) { }

    public virtual DbSet<User> Users { get; set; }
    public virtual DbSet<School> Schools { get; set; }
    public virtual DbSet<Conversation> Conversations { get; set; }
    public virtual DbSet<Participant> Participants { get; set; }
    public virtual DbSet<Message> Messages { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User>()
            .HasIndex(b => new { b.UserCode, b.Email });
    }
}
